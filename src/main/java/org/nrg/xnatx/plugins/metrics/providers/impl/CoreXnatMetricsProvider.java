package org.nrg.xnatx.plugins.metrics.providers.impl;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;

import org.nrg.framework.beans.XnatPluginBean;
import org.nrg.framework.beans.XnatPluginBeanManager;
import org.nrg.xdat.security.helpers.Users;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.XnatAppInfo;
import org.nrg.xnatx.plugins.metrics.dto.CoreXnatMetrics;
import org.nrg.xnatx.plugins.metrics.preferences.MetricsPreferencesBean;
import org.nrg.xnatx.plugins.metrics.providers.MetricsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.nrg.xdat.preferences.SiteConfigPreferences;

@Getter
@Component
public class CoreXnatMetricsProvider extends AbstractMetricsProvider{

    @Autowired
    public CoreXnatMetricsProvider(final NamedParameterJdbcTemplate template,final SiteConfigPreferences siteConfig,
    		final XnatAppInfo info, final XnatPluginBeanManager beanManager, final MetricsPreferencesBean preferences) {
    	_template = template;
    	_siteConfig = siteConfig;
    	_info = info;
    	_beanManager = beanManager;
    	_preferences = preferences;
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public Object getMetrics() throws Exception {
		pluginMap.putAll(Maps.transformValues(_beanManager.getPluginBeans(), new Function<XnatPluginBean,String>(){
			@Override
			public String apply(XnatPluginBean bean) {
			    return bean.getId() + ": " + bean.getName() + " v" + bean.getVersion();
			}
		}));
		
		
		experimentsByDataTypes.putAll(_template.query(EXPERIMENT_BY_DATA_TYPE, new ResultSetExtractor<Map>() {
			@Override
			public Map extractData(ResultSet rs) throws SQLException,DataAccessException{
				HashMap<String, Integer> mapRet = new HashMap<>();
				while(rs.next()){
					mapRet.put(rs.getString("element_name"), rs.getInt("count"));
				}
				return mapRet;
			}
		}));
		
		final CoreXnatMetrics.Builder builder = CoreXnatMetrics.builder();
			builder.version(getCoreXnatMetricsVersion())
				   .projects(_template.queryForObject(PROJECT_COUNT, EmptySqlParameterSource.INSTANCE, Long.class))
			       .subjects(_template.queryForObject(SUBJECT_COUNT, EmptySqlParameterSource.INSTANCE, Long.class))
			       .experiments(_template.queryForObject(EXPERIMENT_COUNT, EmptySqlParameterSource.INSTANCE, Long.class))
			       .dataTypes(0)
			       .experimentsByDataType(experimentsByDataTypes)
			       .users(Lists.newArrayList(Iterables.filter(Users.getUsers(), new Predicate<UserI>() {
				    @Override
				    public boolean apply(final UserI user) {
				    	return user.isEnabled();
				    }
			       	})).size())
			       .platformInfo(_info.getSystemProperty("os.arch"))
			       .plugins(pluginMap)
			       .version(getCoreXnatMetricsVersion())
			       .siteUrl(getUrl())
			       .uuid(_preferences.getSiteUuid());
			       
			       
		return builder.build();
    }
    
    private String getCoreXnatMetricsVersion(){
    	return _beanManager.getPlugin("xnat-metrics").getVersion();
    }
    
    @Override
	public URL getUrl() {
    	URL siteUrl = null;
		try {
			siteUrl = new URL(_siteConfig.getSiteUrl());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return siteUrl;
	}

    private final String id = "CoreXnat";
    private final static List PLATFORM_INFO = new ArrayList<String>();
    private final static Map<String,String> pluginMap = new HashMap<String, String>();
    private final static Map<String,Integer> experimentsByDataTypes = new HashMap<>();
    private final String version = "1.0-SNAPSHOT";
    private final static String PROJECT_COUNT = "SELECT count(*) FROM xnat_projectdata";
    private final static String SUBJECT_COUNT = "SELECT count(*) FROM xnat_subjectdata";
    private final static String EXPERIMENT_COUNT = "SELECT count(*) FROM xnat_experimentdata";
    private final static String EXPERIMENT_BY_DATA_TYPE = "SELECT " +
    													  "m.element_name,count(m.element_name) " +
    													  "FROM " +
    													  "xnat_experimentdata x LEFT JOIN xdat_meta_element m ON x.extension = m.xdat_meta_element_id "+
    													  "GROUP BY m.element_name";
    private final NamedParameterJdbcTemplate _template;
    private final SiteConfigPreferences _siteConfig;
    private final XnatAppInfo _info;
    private final XnatPluginBeanManager _beanManager;
    private final MetricsPreferencesBean _preferences;
	
}
