package org.nrg.xnatx.plugins.metrics.providers.impl;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

import lombok.Getter;

import org.nrg.xnatx.plugins.metrics.providers.MetricsProvider;

@Getter
public abstract class AbstractMetricsProvider implements MetricsProvider {
	
	/**
	 * 
	 */
    private String id;
    
    /**
     * 
     */
    private String version;
    
    /**
     * 
     */
    private URL url;

    /**
     * 
     */
    public abstract Object getMetrics() throws Exception;

}
