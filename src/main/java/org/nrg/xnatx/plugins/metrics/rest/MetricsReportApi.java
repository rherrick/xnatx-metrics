package org.nrg.xnatx.plugins.metrics.rest;

import static org.nrg.xdat.security.helpers.AccessLevel.Admin;

import java.net.MalformedURLException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NotFoundException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xnatx.plugins.metrics.dto.CoreXnatMetrics;
import org.nrg.xnatx.plugins.metrics.entities.MetricsReport;
import org.nrg.xnatx.plugins.metrics.services.ReportService;
import org.nrg.xnatx.plugins.metrics.services.impl.ReportServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.vladmihalcea.hibernate.type.util.Objects;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@XapiRestController
@RequestMapping(value = "/metrics/reports")
@Slf4j
public class MetricsReportApi extends AbstractXapiRestController {

    @Autowired
    protected MetricsReportApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
	    final ReportService reportService) {
	super(userManagementService, roleHolder);
	_reportService = reportService;
    }

    @ApiOperation(value = "Generates and saves new report.", response = String.class, responseContainer = "Map")
    @ApiResponses({ @ApiResponse(code = 200, message = "Report Data Object created and saved successfully"),
	    @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
	    @ApiResponse(code = 403, message = "Not authorized to set site configuration properties."),
	    @ApiResponse(code = 500, message = "Unexpected error") })
    @XapiRequestMapping(value = "/generate", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST, restrictTo = Admin)
    public MetricsReport generateAndSaveReport() throws Exception {
	return _reportService.generateReport();
    }

    @ApiOperation(value = "Returns the report data object.", response = String.class, responseContainer = "Map")
    @ApiResponses({ @ApiResponse(code = 200, message = "Report Data Object created and saved successfully"),
	    @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
	    @ApiResponse(code = 403, message = "Not authorized to set site configuration properties."),
	    @ApiResponse(code = 500, message = "Unexpected error") })
    @XapiRequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET, restrictTo = Admin)
    public List<MetricsReport> retrieveReports() throws Exception {
	return _reportService.getReports();
    }

    @ApiOperation(value = "Returns the report data for the given reportId.", response = MetricsReport.class)
    @ApiResponses({ @ApiResponse(code = 200, message = "Report Data Object created and saved successfully"),
	    @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
	    @ApiResponse(code = 403, message = "Not authorized to set site configuration properties."),
	    @ApiResponse(code = 500, message = "Unexpected error") })
    @XapiRequestMapping(value = "{reportIdString}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET, restrictTo = Admin)
    public List<Object> retrieveOneReportById(
	    @ApiParam(value = "The XNAT Metrics Report Id to be retrieved.", required = true) @PathVariable final String reportIdString)
	    throws Exception {
	long reportId = Long.parseLong(reportIdString);
	return _reportService.getReportById(reportId);
    }

    @ApiOperation(value = "Deletes report for the given reportId.")
    @ApiResponses({ @ApiResponse(code = 200, message = "Report deleted successfully"),
	    @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
	    @ApiResponse(code = 403, message = "Not authorized to set site configuration properties."),
	    @ApiResponse(code = 500, message = "Unexpected error") })
    @XapiRequestMapping(value = "/delete/{reportIdString}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.DELETE, restrictTo = Admin)
    public void deleteReportById(
	    @ApiParam(value = "The XNAT Metrics Report Id to be retrieved.", required = true) @PathVariable final String reportIdString)
	    throws Exception {
	long reportId = Long.parseLong(reportIdString);
	_reportService.deleteReportByReportId(reportId);
    }

    @ApiOperation(value = "Resends report to XNAT restpoint for the given reportId.", response = Long.class)
    @ApiResponses({ @ApiResponse(code = 200, message = "Report deleted successfully"),
	    @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
	    @ApiResponse(code = 403, message = "Not authorized to set site configuration properties."),
	    @ApiResponse(code = 500, message = "Unexpected error") })
    @XapiRequestMapping(value = "/resend/{reportIdString}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT, restrictTo = Admin)
    public long resendReportById(
	    @ApiParam(value = "The XNAT Metrics Report Id to be retrieved.", required = true) @PathVariable final String reportIdString)
	    throws Exception {
	long reportId = Long.parseLong(reportIdString);
	return _reportService.submitReport(reportId);
    }

    private final ReportService _reportService;
}
