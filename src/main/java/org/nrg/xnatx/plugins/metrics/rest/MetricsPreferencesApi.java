package org.nrg.xnatx.plugins.metrics.rest;

import static org.nrg.framework.exceptions.NrgServiceError.ConfigurationError;
import static org.nrg.xdat.security.helpers.AccessLevel.Admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NotFoundException;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xdat.security.user.XnatUserProvider;
import org.nrg.xnatx.plugins.metrics.exception.MetricsException;
import org.nrg.xnatx.plugins.metrics.preferences.MetricsPreferencesBean;
import org.nrg.xnatx.plugins.metrics.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Api
@XapiRestController
@RequestMapping(value = "/metrics/prefs")
@Slf4j
public class MetricsPreferencesApi extends AbstractXapiRestController {

    @Autowired
    protected MetricsPreferencesApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
	    final MetricsPreferencesBean preferences,
	    final XnatUserProvider primaryAdminUserProvider) {
		super(userManagementService, roleHolder);
		_preferences = preferences;
		_primaryAdminUserProvider = primaryAdminUserProvider;
    }

    /**
     * Returns all preferences and values associated with the
     * {@link MetricsPreferencesBean metrics preferences}.
     *
     * @return A map containing all preferences and values for the metrics
     *         preferences.
     */
    @ApiOperation(value = "Returns the full map of Xnat Metrics preferences.", response = String.class, responseContainer = "Map")
    @ApiResponses({ @ApiResponse(code = 200, message = "Site configuration properties successfully retrieved."),
	    @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
	    @ApiResponse(code = 403, message = "Not authorized to set site configuration properties."),
	    @ApiResponse(code = 500, message = "Unexpected error") })
    @XapiRequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET, restrictTo = Admin)
    public Map<String, Object> getXnatMetricsPreferences() {
		log.info("Getting XnatMetricsPreferences");
		_preferences.setAdminName(_primaryAdminUserProvider.get().getFirstname()+" " + _primaryAdminUserProvider.get().getLastname());
		return _preferences;
    }

    /**
     * Gets the value for the specified preference.
     *
     * @param preference The preference to retrieve.
     *
     * @return The value of the specified preference.
     *
     * @throws NotFoundException When the specified preference name doesn't exist in
     *                           the {@link MetricsPreferencesBean metrics
     *                           preferences}.
     */
    @ApiOperation(value = "Returns the value of the specified Xnat Metrics preference.", response = String.class)
    @ApiResponses({ @ApiResponse(code = 200, message = "Xnat Metrics preference value successfully retrieved."),
	    @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
	    @ApiResponse(code = 403, message = "Not authorized to access Xnat Metrics preferences."),
	    @ApiResponse(code = 500, message = "Unexpected error") })
    @XapiRequestMapping(value = "{preference}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET, restrictTo = Admin)
    public String getXnatMetricsPreferenceValue(
	    @ApiParam(value = "The Xnat Metrics preference to retrieve.", required = true) @PathVariable final String preference)
	    throws NotFoundException {
		if (!_preferences.containsKey(preference)) {
		    // log.debug("Preference " + preference + " not found.");
		    throw new NotFoundException("No preference named \"" + preference + "\" was found.");
		}
		return _preferences.getValue(preference);
    }

    /**
     * Sets the value for the specified preference.
     *
     * @param preference The preference to set.
     * @param value      The value to set for the specified preference.
     *
     * @return Returns the previous value for the preference.
     */
    @ApiOperation(value = "Updates the value of the specified Xnat Metrics preference.")
    @ApiResponses({ @ApiResponse(code = 200, message = "Xnat Metrics preference value successfully retrieved."),
	    @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
	    @ApiResponse(code = 403, message = "Not authorized to access Xnat Metrics preferences."),
	    @ApiResponse(code = 500, message = "Unexpected error") })
    @XapiRequestMapping(value = "{preference}", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT, restrictTo = Admin)
    public String setPreference(
	    @ApiParam(value = "The preference to set.", required = true) @PathVariable final String preference,
	    @ApiParam(value = "The value to set for the preference.", required = true) @RequestBody final String value)
	    throws NotFoundException, NrgServiceException {
		if (!_preferences.containsKey(preference)) {
		    throw new NotFoundException("No preference named \"" + preference + "\" was found.");
		}
		try {
		    log.debug("Setting preference {} to the value: {}", preference, value);
		    return _preferences.set(value, preference);
		} catch (InvalidPreferenceName invalidPreferenceName) {
		    throw new NrgServiceException(ConfigurationError, "An error occurred trying to set the \"" + preference
			    + "\" template preference to the value: " + value);
		}
    }

    /**
     * Sets the values for all of the preferences in the submitted map.
     *
     * @param preferences A map of preference names and values.
     * 
     * @throws MetricsException
     */
    @ApiOperation(value = "Updates the value of the specified Xnat Metrics preference.", response = Void.class)
    @ApiResponses({ @ApiResponse(code = 200, message = "Xnat Metrics preference value successfully retrieved."),
	    @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
	    @ApiResponse(code = 403, message = "Not authorized to access Xnat Metrics preferences."),
	    @ApiResponse(code = 500, message = "Unexpected error") })
    @XapiRequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST, restrictTo = Admin)
    public void setPreferences(
	    @ApiParam(value = "The Xnat Metrics preference Map.", required = true) @RequestBody Map<String, Object> preferences)
	    throws MetricsException {
		int identifier = 0;
		try {
		    identifier = ValidationUtils.checkPreferenceMap(preferences);
		} catch (MetricsException e) {
		    log.error("Something went wrong while setting preferenceBean", e);
		}
		switch (identifier) {
		case 1:
		    _preferences.setPiEmail((String) preferences.get(MetricsPreferencesBean.PREF_PI_EMAIL));
		    _preferences.setPiName((String) preferences.get(MetricsPreferencesBean.PREF_PI_NAME));
		    break;
		case 2:
		    _preferences.setSubmitInterval((String) preferences.get(MetricsPreferencesBean.PREF_SUBMIT_INTERVAL));
		    _preferences.setSchedulerEnabled((Boolean) preferences.get(MetricsPreferencesBean.PREF_SCHEDULER_ENABLED));
		    break;
		default:
		    throw new MetricsException("Couldn't be identified as a Preference Map");
		}
	
		log.info("All XnatMetricsPreferencesBean set successfully");

    }

    private final MetricsPreferencesBean _preferences;
    private final XnatUserProvider       _primaryAdminUserProvider;

}
