package org.nrg.xnatx.plugins.metrics.services;

import java.util.List;
import org.nrg.xnatx.plugins.metrics.entities.MetricsReport;
import org.nrg.xnatx.plugins.metrics.providers.MetricsProvider;

public interface ReportService {

    String getUuid();

    List<MetricsProvider> getMetricsProviders();

    List<MetricsReport> getReports() throws Exception;

    MetricsReport generateReport() throws Exception;
    
    List<Object> getReportById(long id) throws Exception;

    long submitReport(long id);
    
    void deleteReportByReportId(long id) throws Exception;
    
}
