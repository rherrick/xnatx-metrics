package org.nrg.xnatx.plugins.metrics.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.nrg.xnat.services.XnatAppInfo;
import org.nrg.xnatx.plugins.metrics.entities.MetricsReport;
import org.nrg.xnatx.plugins.metrics.entities.MetricsReport.MetricsReportBuilder;
import org.nrg.xnatx.plugins.metrics.preferences.MetricsPreferencesBean;
import org.nrg.xnatx.plugins.metrics.providers.MetricsProvider;
import org.nrg.xnatx.plugins.metrics.services.MetricsReportEntityService;
import org.nrg.xnatx.plugins.metrics.services.MetricsTransmissionService;
import org.nrg.xnatx.plugins.metrics.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Data
@Slf4j
public class ReportServiceImpl implements ReportService {

    private final List<MetricsProvider> _metricsProviders = new ArrayList<MetricsProvider>();
    private List<Object> _metrics = new ArrayList<Object>();
    private final MetricsReportEntityService _metricsReportEntityService;
    private final MetricsTransmissionService _metricsTransmissionService;
    
    @Autowired
    public ReportServiceImpl(final MetricsReportEntityService metricsReportEntityService,
	    final List<MetricsProvider> metricsProviders, final MetricsPreferencesBean preferences,
	    final MetricsTransmissionService metricsTransmissionService) {
	_metricsProviders.addAll(metricsProviders);
	_metricsReportEntityService = metricsReportEntityService;
	_preferences = preferences;
	_metricsTransmissionService = metricsTransmissionService;
    }

    @Override // Needs to be implemented.
    public String getUuid() {
	return _preferences.getSiteUuid();
    }

    @Override
    public List<MetricsReport> getReports() {
	return _metricsReportEntityService.findAllReports();
    }

    @Override
    @Transactional
    public MetricsReport generateReport() throws Exception {
	_metrics.clear();
	final MetricsReportBuilder builder = MetricsReport.builder();
	for (MetricsProvider metricsProvider : _metricsProviders) {
	    _metrics.add(metricsProvider.getMetrics());
	}
	final ResponseEntity<String> response = _metricsTransmissionService.pushReportToAccessPoint(_metrics);
	if (Objects.nonNull(response)) {
	    builder.status(response.getStatusCodeValue()).statusText(response.getStatusCode().name());
	} else {
	    builder.status(0).statusText("RestClient Exception");
	}
	builder.report(_metrics).attempts(1).lastAttempt(new Date()).version("1");
	try {
	    final MetricsReport generatedMetricsReport = _metricsReportEntityService.create(builder.build());
	    log.info("MetricsReport generated and saved in the Database successfully.");
	    return generatedMetricsReport;
	} catch (Exception e) {
	    log.error(e.getMessage());
	    return null;
	}
    }

    @Override
    @Transactional
    public long submitReport(long id) {
	final MetricsReport retrievedReport = _metricsReportEntityService.findReportById(id);
	Objects.requireNonNull(retrievedReport, "No Report for the given reportId:  \"" + id + "\" was found.");
	final ResponseEntity<String> response = _metricsTransmissionService.pushReportToAccessPoint(retrievedReport.getReport());
	if (Objects.nonNull(response)) {
	    retrievedReport.setStatus(response.getStatusCodeValue());
	    retrievedReport.setStatusText(response.getStatusCode().name());
	} else {
	    retrievedReport.setStatus(0);
	    retrievedReport.setStatusText("RestClient Exception");

	}
	retrievedReport.setAttempts(retrievedReport.getAttempts() + 1);
	retrievedReport.setLastAttempt(new Date());
	_metricsReportEntityService.update(retrievedReport);
	
	return retrievedReport.getId();
    }

    @Override
    public List<MetricsProvider> getMetricsProviders() {
	return _metricsProviders;
    }

    @Override
    public List<Object> getReportById(long id) throws Exception {
	final MetricsReport retrievedReport = _metricsReportEntityService.findReportById(id);
	Objects.requireNonNull(retrievedReport, "No Report for the given reportId:  \"" + id + "\" was found.");
	return retrievedReport.getReport();
    }

    @Override
    @Transactional
    public void deleteReportByReportId(long id) throws Exception {
	final MetricsReport retrievedReport = _metricsReportEntityService.findReportById(id);
	Objects.requireNonNull(retrievedReport, "No Report for the given reportId:  \"" + id + "\" was found.");
	_metricsReportEntityService.delete(retrievedReport.getId());
	log.info("Metrics Report Entity with id " + id + " is deleted.");
    }

    private final MetricsPreferencesBean _preferences;
}
